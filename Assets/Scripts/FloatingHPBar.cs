﻿using UnityEngine;

public class FloatingHPBar : MonoBehaviour
{
    [SerializeField] private GameObject rootCanvas;
    [SerializeField] private GameObject prefabHPBar;

    [HideInInspector] public HPBarSlider hpBar;
    public bool showHPBar = true;

    void Awake()
    {
        GameObject newBar = Instantiate(prefabHPBar, new Vector3(0, 0, 0), Quaternion.identity);
        newBar.transform.SetParent(rootCanvas.transform, false);
        hpBar = newBar.GetComponent<HPBarSlider>();
    }

    void Update()
    {
        hpBar.gameObject.SetActive(showHPBar);
    }

    public void AdjustHPBar(float currentHealth, float maxHealth)
    {
        hpBar.slider.value = currentHealth / maxHealth;
    }

    public void SetColor(Player owner)
    {
        hpBar.SetColor(owner);
    }
}
