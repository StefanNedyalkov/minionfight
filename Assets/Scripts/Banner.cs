﻿public class Banner : Unit
{
    public HPBar hpBar;

    protected override void Update()
    {
        base.Update();

        hpBar.AdjustHPBar(currentHealth, maxHealth);
    }
}