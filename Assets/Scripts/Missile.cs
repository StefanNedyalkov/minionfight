﻿using UnityEngine;

public class Missile : MonoBehaviour
{
	public float missileSpeed;
	public Unit missileTarget;
	public float missileDamage;

	private void Update ()
	{
		if (missileTarget == null)
		{
			Destroy (gameObject);
		}

		gameObject.transform.LookAt (missileTarget.gameObject.transform.GetComponent<SphereCollider> ().bounds.center);
		gameObject.transform.Translate (Vector3.forward * missileSpeed * Time.deltaTime);
	}

	void OnTriggerEnter (Collider otherObject)
	{
		if (otherObject.gameObject == missileTarget.gameObject)
		{
			Destroy (gameObject, 0.4f);
			missileTarget.TakeDamage (missileDamage);
		}
	}

	public void SetStats (float damage, Unit target)
	{
		missileTarget = target;
		missileDamage = damage;
	}
}
