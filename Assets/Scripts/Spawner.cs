﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Player player;

    public float spawnCheckInterval = 1.5f;
    public GameObject fullGrid;
    public GameObject defenceGrid;
    public List<MinionData> aiMinionsInHand = new List<MinionData>();

    private bool _shouldSpawn;

    private float START_TIME = 3f;
    private int MAX_UNITS_IN_HAND = 4;

    private void Start()
    {
        AddMinionsToHand();

        GameUtils.Instance.Cooldown(START_TIME, () =>
        {
            _shouldSpawn = true;
        });
    }

    private void Update()
    {
        if (_shouldSpawn)
        {
            if (GameManager.Instance.redBanner.IsInCombat)
            {
                SpawnDefensiveUnit();
            }
            else
            {
                SpawnUnit();
            }
        }
    }

    private void AddMinionsToHand()
    {
        while (aiMinionsInHand.Count < MAX_UNITS_IN_HAND)
        {
            int randomIndex = Random.Range(0, CardManager.Instance.minionDataList.minions.Count);
            aiMinionsInHand.Add(CardManager.Instance.minionDataList.minions[randomIndex]);
        }
    }

    private void RemoveMinionFromHand(MinionData minionData)
    {
        aiMinionsInHand.Remove(minionData);
        AddMinionsToHand();
    }

    private MinionData GetRandomMinionFromHand()
    {
        int randomIndex = Random.Range(0, aiMinionsInHand.Count);

        return aiMinionsInHand[randomIndex];
    }

    private void SpawnDefensiveUnit()
    {
        _shouldSpawn = false;

        var randomMinionData = GetRandomMinionFromHand();

        bool canSpawnMinion = randomMinionData.manaCost <= player.currentMana;
        if (canSpawnMinion)
        { 
            Vector3 randomPos = GameUtils.Instance.RandomPointInBox(defenceGrid.transform.position, defenceGrid.GetComponent<Renderer>().bounds.size);
            GameObject minionGameObject = Instantiate(randomMinionData.normalPrefab, randomPos, Quaternion.Euler(0, -90, 0));
            Minion minion = minionGameObject.GetComponent<Minion>();
            minion.SetStats(randomMinionData, player);
            player.RemoveMana(randomMinionData.manaCost);
            RemoveMinionFromHand(randomMinionData);
        }

        GameUtils.Instance.Cooldown(spawnCheckInterval, () =>
        {
            _shouldSpawn = true;
        });
    }

    private void SpawnUnit()
    {
        _shouldSpawn = false;

        var randomMinionData = GetRandomMinionFromHand();

        bool canSpawnMinion = randomMinionData.manaCost <= player.currentMana;
        if (canSpawnMinion)
        {
            Vector3 randomPos = GameUtils.Instance.RandomPointInBox(fullGrid.transform.position, fullGrid.GetComponent<Renderer>().bounds.size);
            GameObject minionGameObject = Instantiate(randomMinionData.normalPrefab, randomPos, Quaternion.Euler(0, -90, 0));
            Minion minion = minionGameObject.GetComponent<Minion>();
            minion.SetStats(randomMinionData, player);
            player.RemoveMana(randomMinionData.manaCost);
            RemoveMinionFromHand(randomMinionData);
        }

        GameUtils.Instance.Cooldown(spawnCheckInterval, () =>
        {
            _shouldSpawn = true;
        });
    }
}
