﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour
{
    public Text manaText;
    public Image cardImage;
    public Image manaImage;
    public Color outOfManaColor;

    private bool isTweening;

    public MinionData Minion { get; private set; }

    public void AddNewCard()
    {
        Minion = CardManager.Instance.minionDataList.minions[Random.Range(0, CardManager.Instance.minionDataList.minions.Count)];
        manaText.text = Minion.manaCost.ToString();
        cardImage.sprite = Minion.minionSprite;
    }

    public bool CanUseCard()
    {
        if (Minion.manaCost > GameManager.Instance.bluePlayer.currentMana)
        {
            OutOfManaEffect();

            return false;
        }

        return true;
    }

    private void OutOfManaEffect()
    {
        if (isTweening)
            return;

        isTweening = true;
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(manaImage.transform.DOMoveY(manaImage.transform.position.y + 10, 0.1f));
        mySequence.Join(manaImage.DOColor(outOfManaColor, 0));
        mySequence.Append(manaImage.transform.DOMoveY(manaImage.transform.position.y, 0.1f));
        mySequence.Append(manaImage.DOColor(new Color(255, 255, 255), 0.2f));
        mySequence.AppendCallback(() =>
        {
            isTweening = false;
        });
    }
}