﻿using UnityEngine;

public class AnimatorHelper : MonoBehaviour
{
	private Minion minionScript;

	private void Awake ()
	{
		minionScript = transform.parent.gameObject.GetComponent<Minion> ();
	}

	private void MeleeAttackEnd()
	{
		minionScript.OnMeleeAttackEnd();
	}

	private void RangeAttackEnd()
	{
		minionScript.OnRangeAttackEnd();
	}
}
