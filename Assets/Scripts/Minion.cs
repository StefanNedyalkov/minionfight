﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Minion : Unit
{
    public MinionData minionData;

    private FloatingHPBar hpBar;
    private NavMeshAgent agent;
    private Animator animator;
    private Banner enemyBanner;
    private Unit currentTarget;
    private bool isSpawning = true;
    private bool canAttack = true;

    private void Awake()
    {
        hpBar = GetComponent<FloatingHPBar>();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
    }

    protected override void Start()
    {
        //base.Start();

        enemyBanner = owner == GameManager.Instance.bluePlayer ? GameManager.Instance.redBanner : GameManager.Instance.blueBanner;
        currentHealth = minionData.health;
        maxHealth = minionData.health;
        hpBar.SetColor(owner);
        hpBar.AdjustHPBar(currentHealth, maxHealth);
        currentTarget = enemyBanner;
        agent.destination = currentTarget.transform.position;
        agent.stoppingDistance = minionData.meleeRange;

        GameUtils.Instance.Cooldown(0.75f, () =>
        {
            isSpawning = false;
        });
    }

    protected override void Update()
    {
        base.Update();

        animator.SetFloat("speed", agent.speed);
        animator.SetBool("isAlive", IsAlive);
        hpBar.AdjustHPBar(currentHealth, maxHealth);

        if (!IsAlive || isSpawning)
            return;

        if (currentTarget == null)
        {
            agent.speed = 0;
            agent.ResetPath();
            LookForCloserTarget();
        }
        else
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("attack") && !animator.GetCurrentAnimatorStateInfo(0).IsName("rangeAttack"))
            {
                agent.speed = minionData.speed;
                agent.destination = currentTarget.transform.position;
            }

            if (InMeleeOfTarget())
            {
                agent.speed = 0;
                if (GameUtils.Instance.IsLookingAtTarget(gameObject.transform, currentTarget.transform, 0.95f))
                {
                    MeleeAttack();
                }
                else
                {
                    GameUtils.Instance.RotateTowards(gameObject.transform, currentTarget.transform, agent.angularSpeed);
                }
            }
            else if (InRangeOfTarget())
            {
                agent.speed = 0;
                if (GameUtils.Instance.IsLookingAtTarget(gameObject.transform, currentTarget.transform, 0.95f))
                {
                    RangeAttack();
                }
                else
                {
                    GameUtils.Instance.RotateTowards(gameObject.transform, currentTarget.transform, agent.angularSpeed);
                }
            }
            else
            {
                LookForCloserTarget();
            }
        }
    }

    public void SetStats(MinionData minionData, Player owner)
    {
        this.owner = owner;
        this.minionData = minionData;
    }

    protected override void Die()
    {
        hpBar.showHPBar = false;
        agent.enabled = false;
        owner.unitsLost++;

        StartCoroutine(DecomposeBody(2.5f, 3f));
    }

    private IEnumerator DecomposeBody(float time, float duration)
    {
        yield return new WaitForSeconds(time);
        StartCoroutine(GameUtils.Instance.MoveOverSeconds(gameObject, gameObject.transform.position - new Vector3(0, 1, 0), duration, () =>
        {
            Destroy(gameObject);
        }));
    }

    private bool InMeleeOfTarget()
    {
        if (!agent.pathPending)
        {
            if (agent.remainingDistance <= agent.stoppingDistance && currentTarget.IsAlive)
            {
                return true;
            }
        }

        return false;
    }

    private bool InRangeOfTarget()
    {
        if (minionData.rangeRange > 0)
        {
            if (!agent.pathPending)
            {
                if (agent.remainingDistance <= minionData.rangeRange && currentTarget.IsAlive)
                {
                    return true;
                }
            }
        }

        return false;
    }

    private void LookForCloserTarget()
    {
        List<Unit> enemiesInRange = new List<Unit>();
        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, minionData.acquisitionRange);
        foreach (Collider hit in hitColliders)
        {
            if (hit.gameObject.GetComponent<Unit>() == null)
            {
                continue;
            }
            Unit foundMinion = hit.gameObject.GetComponent<Unit>();
            if (foundMinion.owner != owner && foundMinion.IsAlive)
            {
                enemiesInRange.Add(foundMinion);
            }
        }

        if (enemiesInRange.Count > 0)
        {
            currentTarget = GetClosestEnemy(enemiesInRange);
        }
        else
        {
            currentTarget = enemyBanner;
        }
    }

    private Unit GetClosestEnemy(List<Unit> enemies)
    {
        Unit closestEnemy = null;
        float minDist = Mathf.Infinity;
        foreach (Unit minion in enemies)
        {
            float dist = Vector3.Distance(minion.gameObject.transform.position, gameObject.transform.position);
            if (dist < minDist)
            {
                closestEnemy = minion;
                minDist = dist;
            }
        }

        return closestEnemy;
    }

    private void MeleeAttack()
    {
        if (canAttack)
        {
            animator.SetBool("isMeleeAttacking", true);
            canAttack = false;
        }
    }

    private void RangeAttack()
    {
        if (canAttack)
        {
            animator.SetBool("isRangeAttacking", true);
            canAttack = false;
        }
    }

    public void OnMeleeAttackEnd()
    {
        currentTarget.TakeDamage(minionData.meleeDamage);

        animator.SetBool("isMeleeAttacking", false);
        GameUtils.Instance.Cooldown(minionData.meleeAttackDelay, () =>
        {
            canAttack = true;
        });
    }

    public void OnRangeAttackEnd()
    {
        GameObject missile = Instantiate(minionData.missilePrefab, gameObject.transform.GetComponent<SphereCollider>().bounds.center, Quaternion.identity);
        missile.GetComponent<Missile>().SetStats(minionData.rangeDamage, currentTarget);

        animator.SetBool("isRangeAttacking", false);
        GameUtils.Instance.Cooldown(minionData.rangeAttackDelay, () =>
        {
            canAttack = true;
        });
    }
}