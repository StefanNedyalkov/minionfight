﻿using UnityEngine;

/// <summary>
/// WORLD SPACE CANVAS FACES PLAYER AT ALL TIMES
/// </summary>
public class CameraFacingBillboard : MonoBehaviour
{
    private Camera m_Camera;

	void Awake()
	{
		m_Camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}
    void Start()
    {
		LookAtCamera ();
    }

    void Update()
    {
		LookAtCamera ();
    }

	void LookAtCamera()
	{
		transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward, m_Camera.transform.rotation * Vector3.up);
	}
}