﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public GameObject topPanel;
    public GameObject bottomPanel;
    public GameObject pauseMenu;
    public GameObject cogwheelButton;
    public Text timerText;
    public GameOverMenu gameOverMenu;

    private bool isTweening;

    private void Start()
    {
        topPanel.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 100);
        bottomPanel.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -350);
        cogwheelButton.GetComponent<RectTransform>().anchoredPosition = new Vector2(-20, 100);

        ShowUI();
    }

    public void ShowPauseMenu()
    {
        if (isTweening)
            return;

        Time.timeScale = 0;
        HideUI(() =>
        {
            pauseMenu.SetActive(true);
        });
    }

    public void HidePauseMenu()
    {
        if (isTweening)
            return;

        pauseMenu.SetActive(false);
        ShowUI(() =>
        {
            Time.timeScale = 1;
        });
    }

    public void RestartGame()
    {
        if (isTweening)
            return;

        Time.timeScale = 1;
        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);
    }

    public void ExitToMenu()
    {
        if (isTweening)
            return;

        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void ExitGame()
    {
        if (isTweening)
            return;

        Application.Quit();
    }

    public void GameOverScreen(Player winner)
    {
        Time.timeScale = 0;
        HideUI(() =>
        {
            gameOverMenu.SetTexts(winner, timerText.text);
            gameOverMenu.gameObject.SetActive(true);
        });
    }

    public void UpdateTimer(float timerCount)
    {
        int minutes = Mathf.FloorToInt(timerCount / 60F);
        int seconds = Mathf.FloorToInt(timerCount - minutes * 60);
        timerText.text = string.Format("{0:0}:{1:00}", minutes, seconds);
    }

    private void ShowUI(Action callback = null)
    {
        isTweening = true;
        Sequence mySequence = DOTween.Sequence().SetUpdate(true);
        mySequence.Append(topPanel.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 0.3f));
        mySequence.Join(bottomPanel.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 0), 0.3f));
        mySequence.Join(cogwheelButton.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-20, -20), 0.3f));
        mySequence.AppendCallback(() =>
        {
            isTweening = false;
            if (callback != null)
                callback();
        });
    }

    private void HideUI(Action callback = null)
    {
        isTweening = true;
        Sequence mySequence = DOTween.Sequence().SetUpdate(true);
        mySequence.Append(topPanel.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, 100), 0.3f));
        mySequence.Join(bottomPanel.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0, -350), 0.3f));
        mySequence.Join(cogwheelButton.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-20, 100), 0.3f));
        mySequence.AppendCallback(() =>
        {
            isTweening = false;
            if (callback != null)
                callback();
        });
    }
}