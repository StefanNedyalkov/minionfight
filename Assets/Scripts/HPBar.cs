﻿using UnityEngine;

public class HPBar : MonoBehaviour
{
	public HPBarSlider hpBar;

	public void AdjustHPBar (float currentHealth, float maxHealth)
	{
		hpBar.slider.value = currentHealth / maxHealth;
		hpBar.SetText (currentHealth);
	}
}
