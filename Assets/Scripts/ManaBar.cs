﻿using UnityEngine;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour
{
    public Slider currentManaSlider;
    public Slider usedManaSlider;
    public Text manaText;

    void Update()
    {
        currentManaSlider.value = GameManager.Instance.bluePlayer.currentMana;
        manaText.text = (Mathf.Floor(currentManaSlider.value)).ToString();
    }

    public void ShowUsedManaSlider(int mana)
    {
        usedManaSlider.gameObject.SetActive(true);
        usedManaSlider.value = mana;
    }

    public void HideUsedManaSlider()
    {
        usedManaSlider.gameObject.SetActive(false);
    }
}
