﻿using UnityEngine;
using UnityEngine.UI;

public class HPBarSlider : MonoBehaviour
{
    [SerializeField] private Image fillImage;
    public Slider slider;
    public Text label;

    public void SetColor(Player owner)
    {
        fillImage.color = owner.color;
    }

    public void SetText(float value)
    {
        label.text = ((int)value).ToString();
        if (value < 0)
        {
            label.text = "0";
        }
    }
}
