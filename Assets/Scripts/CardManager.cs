﻿using System.Collections.Generic;

public class CardManager : Singleton<CardManager>
{
    public MinionDataList minionDataList;
    public List<Card> cards;
    public ManaBar manaBar;

    private void Start()
    {
        foreach (var card in cards)
        {
            card.AddNewCard();
        }
    }

    public void DragCard(Card card)
    {
        if (!card.CanUseCard())
            return;

        DragAndDropManager.Instance.SpawnGhost(card);
        manaBar.ShowUsedManaSlider(card.Minion.manaCost);
    }

    public void HideUsedManaBarSlide()
    {
        manaBar.HideUsedManaSlider();
    }
}