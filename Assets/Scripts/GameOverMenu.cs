﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    public Text topText;
    public Text time;
    public Text kills;
    public Text deaths;

    public void SetTexts(Player winner, string time)
    {
        topText.text = winner == GameManager.Instance.bluePlayer ? "YOU WON" : "YOU LOST";
        this.time.text = string.Format("Time elapsed: {0}", time);
        kills.text = string.Format("Minions killed: {0}", winner.unitsKilled);
        deaths.text = string.Format("Minions lost: {0}", winner.unitsLost);
    }
}