﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public float startingMana = 2f;
    public float currentMana;
    public float manaPerSecond = 0.3f;

    public Color color;

    public int unitsKilled;
    public int unitsLost;

    public bool isAlive = true;

    private const int MIN_MANA = 0;
    private const int MAX_MANA = 10;

    private void Start()
    {
        currentMana = startingMana;
    }

    private void Update()
    {
        AddMana(manaPerSecond * Time.deltaTime);
    }

    private void AddMana(float mana)
    {
        currentMana = Mathf.Clamp(currentMana + mana, MIN_MANA, MAX_MANA);
    }

    public void RemoveMana(float mana)
    {
        currentMana = Mathf.Clamp(currentMana - mana, MIN_MANA, MAX_MANA);
    }
}