﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MinionDataList : ScriptableObject
{
    public List<MinionData> minions;
}
