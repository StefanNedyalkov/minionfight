﻿using UnityEngine;

public class Unit : MonoBehaviour
{
    public Player owner;

    public float maxHealth;
    public float currentHealth;

    private float combatDuration;

    private const float TIME_IN_COMBAT = 3f;

    public bool IsAlive
    {
        get
        {
            return currentHealth > 0;
        }
    }

    public bool IsInCombat
    {
        get
        {
            return combatDuration == 0;
        }
    }

    protected virtual void Start()
    {
        currentHealth = maxHealth;
    }

    protected virtual void Update()
    {
        combatDuration = Mathf.Clamp(combatDuration - Time.deltaTime, 0, TIME_IN_COMBAT);
    }

    public void TakeDamage(float damage)
    {
        if (!IsAlive)
            return;

        currentHealth = Mathf.Clamp(currentHealth - damage, 0, maxHealth);

        combatDuration = TIME_IN_COMBAT;

        if (!IsAlive)
        {
            Die();
        }
    }

    protected virtual void Die()
    {
        Destroy(gameObject);
    }
}