﻿using System;
using UnityEngine;

[CreateAssetMenu]
[Serializable]
public class MinionData : ScriptableObject
{
    public Sprite minionSprite;
    public GameObject normalPrefab;
    public GameObject ghostPrefab;
    public int manaCost;
    [Header("Combat Stats")]
    public float health;
    public float speed;
    public float acquisitionRange;
    [Space(8)]
    public float meleeRange;
    public float meleeAttackDelay;
    public float meleeDamage;
    [Space(8)]
    public float rangeRange;
    public float rangeAttackDelay;
    public float rangeDamage;
    public GameObject missilePrefab;
}