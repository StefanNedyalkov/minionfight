﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameUtils : Singleton<GameUtils>
{
    public void RotateTowards(Transform currentTransform, Transform targetTransform, float angularSpeed)
    {
        Vector3 direction = (targetTransform.position - currentTransform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        currentTransform.rotation = Quaternion.RotateTowards(currentTransform.rotation, lookRotation, Time.deltaTime * angularSpeed);
    }

    public void RotateTowardsOverTime(Transform currentTransform, Transform targetTransform, float time)
    {
        Vector3 direction = (targetTransform.position - currentTransform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        currentTransform.rotation = Quaternion.Slerp(currentTransform.rotation, lookRotation, Time.deltaTime * time);
    }

    public bool IsLookingAtTarget(Transform currentTransform, Transform targetTransform, float threshold)
    {
        Vector3 dirFromAtoB = (targetTransform.position - currentTransform.position).normalized;
        float dotProd = Vector3.Dot(dirFromAtoB, currentTransform.forward);

        if (dotProd > threshold)
        {
            return true;
        }

        return false;
    }

    public IEnumerator MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds, Action callback)
    {
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while (elapsedTime < seconds)
        {
            objectToMove.transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        objectToMove.transform.position = end;
        callback();
    }

    public Vector3 RandomPointInBox(Vector3 center, Vector3 size)
    {
        return center + new Vector3(
            (Random.value - 0.5f) * size.x,
            (Random.value - 0.5f) * size.y,
            (Random.value - 0.5f) * size.z
        );
    }

    public void Cooldown(float cooldown, Action callback)
    {
        StartCoroutine(CooldownEnumerator(cooldown, callback));
    }

    private IEnumerator CooldownEnumerator(float cooldown, Action callback)
    {
        yield return new WaitForSeconds(cooldown);
        callback();
    }
}