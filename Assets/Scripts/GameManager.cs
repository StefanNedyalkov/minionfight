﻿using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public Player bluePlayer;
    public Player redPlayer;

    [Space (10)]
    public Banner blueBanner;
    public Banner redBanner;

    private bool isGameOver;
    private float timerCount;

    private void Update()
    {
        if (isGameOver)
            return;

        timerCount += Time.deltaTime;
        UIManager.Instance.UpdateTimer(timerCount);

        if (!bluePlayer.isAlive)
        {
            UIManager.Instance.GameOverScreen(redPlayer);
            isGameOver = true;
        }
        else if (!redPlayer.isAlive)
        {
            UIManager.Instance.GameOverScreen(bluePlayer);
            isGameOver = true;
        }
    }
}