﻿using UnityEngine;

public class DragAndDropManager : Singleton<DragAndDropManager>
{
    public LayerMask acceptableLayer;
    public GameObject arenaGrid;

    private Card cardDragged;
    private GameObject currentGhost;
    private bool canSpawnMinion;
    private Vector3 spawnPosition;

    public void SpawnGhost(Card card)
    {
        cardDragged = card;

        currentGhost = Instantiate(cardDragged.Minion.ghostPrefab, Vector3.zero, Quaternion.Euler(0, 90, 0));
        currentGhost.name = "Ghost: (" + cardDragged.Minion.name + ")";
        currentGhost.SetActive(false);

        arenaGrid.SetActive(true);
    }

    public void Update()
    {
        if (Input.GetMouseButton(0) && currentGhost)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 50, acceptableLayer))
            {
                spawnPosition = new Vector3(hit.point.x, hit.collider.transform.position.y, hit.point.z);
                currentGhost.transform.position = spawnPosition;
                currentGhost.SetActive(true);
                canSpawnMinion = true;
            }
            else
            {
                currentGhost.SetActive(false);
                canSpawnMinion = false;
            }
        }

        if (Input.GetMouseButtonUp(0) && currentGhost)
        {
            if (canSpawnMinion)
            {
                GameObject newMinion = Instantiate(cardDragged.Minion.normalPrefab, spawnPosition, Quaternion.Euler(0, 90, 0));

                newMinion.GetComponent<Minion>().SetStats(cardDragged.Minion, GameManager.Instance.bluePlayer);
                GameManager.Instance.bluePlayer.RemoveMana(cardDragged.Minion.manaCost);
                cardDragged.AddNewCard();
            }

            Destroy(currentGhost);
            CardManager.Instance.HideUsedManaBarSlide();
            arenaGrid.SetActive(false);
        }
    }
}